/**
 * Car class
 * @constructor
 * @param {String} model
 */

//  Create an instance, accelerate twice, brake once, and console log the instance.toString()
class Car {
    constructor(brand) {
      this.carname = brand;
      this.currentSpeed = 0 

    }
   tostring() {
      return 'I have a ' + this.carname;
    }
    accelerate() {
        this.currentSpeed ++ ;
    }
    break() {
        this.currentSpeed -- ; 
    }
  }
//   class Model extends Car {
//     constructor(brand, mod) {
//       super(brand);
//       this.model = mod;
//     }
//     show() {
//       return this.present() + ', it is a ' + this.model;
//     }
//   }
  let myCar = new Car('Ford'); 
  console.log(myCar.tostring());

/**
 * ElectricCar class 
 * @constructor
 * @param {String} model
 */

//  Create an instance, accelerate twice, brake once, and console log the instance.toString()
    class ElectirCar extends Car {
        constructor(brand) {
        super(brand) }
    tostring() {
        return 'This is a supper Electric car ' + this.carname ;
    }
    }
        
    
    let evCar = new ElectirCar ('Ford');
    console.log(evCar.tostring());